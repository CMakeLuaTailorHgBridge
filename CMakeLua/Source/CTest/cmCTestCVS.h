/*=========================================================================

  Program:   CMake - Cross-Platform Makefile Generator
  Module:    $RCSfile: cmCTestCVS.h,v $
  Language:  C++
  Date:      $Date: 2009-02-25 19:42:45 $
  Version:   $Revision: 1.2 $

  Copyright (c) 2002 Kitware, Inc. All rights reserved.
  See Copyright.txt or http://www.cmake.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef cmCTestCVS_h
#define cmCTestCVS_h

#include "cmCTestVC.h"

/** \class cmCTestCVS
 * \brief Interaction with cvs command-line tool
 *
 */
class cmCTestCVS: public cmCTestVC
{
public:
  /** Construct with a CTest instance and update log stream.  */
  cmCTestCVS(cmCTest* ctest, std::ostream& log);

  virtual ~cmCTestCVS();

private:
  // Implement cmCTestVC internal API.
  virtual bool UpdateImpl();
  virtual bool WriteXMLUpdates(std::ostream& xml);

  // Update status for files in each directory.
  class Directory: public std::map<cmStdString, PathStatus> {};
  std::map<cmStdString, Directory> Dirs;

  std::string ComputeBranchFlag(std::string const& dir);
  void LoadRevisions(std::string const& file, const char* branchFlag,
                     std::vector<Revision>& revisions);
  void WriteXMLDirectory(std::ostream& xml, std::string const& path,
                         Directory const& dir);

  // Parsing helper classes.
  class UpdateParser;
  class LogParser;
  friend class UpdateParser;
  friend class LogParser;
};

#endif
